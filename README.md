# machine-learning-life-science

Learning materials and references for M.Sc. course on machine learning applied to life science.

The PDF of the course will be updated before each course.
Keep in mind that most of the course is on blackboard, so if you miss the,
course, ask a friend to put you up to speed.

# Lecture 1:
First lecture is about gene expression and machine learning.
You'll find the material of the first lecture in this document:
 course1_gene_expression.pdf

## Resources:
Gene expression inference with deep learning:
https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4908320/

Protein Secondary Structure Prediction with Long Short Term Memory Networks:
https://arxiv.org/pdf/1412.7828v2.pdf

Recent Advances of Deep Learning in Bioinformatics and Computational Biology:
https://www.frontiersin.org/articles/10.3389/fgene.2019.00214/full

# Lecture 2
The second lecture explores problems and methods in image recognition.
Segmentation, localization, classification and object detection
methods will be explored.

## Resources:

Stanford video course (Detection, Segmentation...):
https://www.youtube.com/watch?v=nDPWywWRIRo
And the pdf: http://cs231n.stanford.edu/slides/2017/cs231n_2017_lecture11.pdf

U-net:
https://arxiv.org/pdf/1505.04597.pdf

Object detection Review:
https://arxiv.org/pdf/1807.05511.pdf

# Lecture 3
The third lecture is an overview of weakly-supervised learning challenges and
related issues: pure semi-supervised, active learning, transductive learning,
learning by label proportion, federated learning and byzantine data.

## Resources:

Great comics from google about federated learning:
https://federated.withgoogle.com/

Distributed Statistical Machine Learning in Adversarial Settings:
Byzantine Gradient Descent:
https://arxiv.org/pdf/1705.05491.pdf

Federated Multitask learning:
https://arxiv.org/pdf/1705.10467.pdf

On Learning from Label Proportions:
https://arxiv.org/pdf/1402.5902.pdf

Learning from label proportion on high-dimensional data:
http://it.uibe.edu.cn/docs/20190113214042680119.pdf

# Lecture 4
The last lecture will explore magnetic resonance data (especially NMR) and the
challenge of peak picking.

## Resources:

PICKY: a novel SVD-based NMR spectra peak picking method:
https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2687979/

Laplace Inversion of Low-Resolution NMR Relaxometry Data Using Sparse
 Representation Methods:
 https://web.stanford.edu/group/SOL/papers/CMR21263.pdf

Bayesian Peak Picking for NMR Spectra:
https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4411369/

NMRNet: a deep learning approach to automated peak picking of protein
 NMR spectra:
 https://www.ncbi.nlm.nih.gov/pubmed/29547986

# Evaluation of the course

In order to evaluate your understanding of the course, you will have to solve
the Medical Segmentation Decathlon challenge!

http://medicaldecathlon.com/

You will have to build a team made of 3 or 4 students

The output expected will be:
- a clean code (clean = you made some effort so that I can understand the
   code!). I recommend to provide an access to a gitlab or github repo.
- your score published on the website of the Decathlon Challenge
- a written report of 3 to 5 pages that formalize the problem to be solved, the
 method you decided to use (as well as any initiative you decided to take to
 improve the results) and some cogent argument of the performance of your
 estimator.
- the feedback form filled: https://forms.gle/7yCNMiqJAVGxgx6S7

 *Deadline: on January 31, 2020*, you will have to send me an email with your
  written report, your team's name (in the Segmentation Medical Decathlon
  Challenge) and an access to your codes.

# Miscellaneous:
We are currently offering an *internship position* for Machine Learning engineer
for image recognition for plant science. For more information, see here:
http://bit.ly/2q7SMB4
